#
include < TVout.h > #include < video_gen.h > #include < fontALL.h >

  TVout TV;

int player1Score = 0;
int player2Score = 0;
int neededScore = 3;
int maxPositions[2] = {
  118,
  96
};
int ballPosition[2] = {
  maxPositions[0] / 2,
  maxPositions[1] / 2
};
int ballDirection = 0;
int ballEndPosition[2] = {
  0,
  0
};

int batSize[2] = {
  4,
  30
};

int player1Input = A0;
int player2Input = A1;
int startButton = 2;
int startButtonState = 0;
int player1InputVal = 0;
int player2InputVal = 0;

bool start = false;
bool newRound = false;
int state = -1;
bool startBlink = true;

void setup() {
  // put your setup code here, to run once:
  TV.begin(PAL, 120, 96);
  Serial.begin(9600);
  TV.select_font(font4x6);
  pinMode(startButton, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  startButtonState = digitalRead(startButton);
  switch (state) {
  case -1:
    intro();
    break;
  case 0:
    startUp();
    break;
  case 1:
    game();
    break;
  case 2:
    winner();
    break;
  }

}

void startUp() {
  if (!start == true) {
    if (startButtonState == HIGH) {
      start = true;
    } else {
      start = false;
    }
    TV.select_font(font4x6);
    TV.println(10, 30, "LET US PLAY");
    TV.select_font(font8x8);
    TV.println(10, 60, "PONG");
    delay(100);

  } else {
    player1Score = 0;
    player2Score = 0;
    newRound = true;
    TV.select_font(font8x8);
    TV.println(20, 30, "GET READY");
    delay(50000);
    TV.clear_screen();
    TV.println(60, 30, "3");
    delay(50000);
    TV.clear_screen();
    TV.println(60, 30, "2");
    delay(50000);
    TV.clear_screen();
    TV.println(60, 30, "1");
    delay(50000);
    TV.clear_screen();
    TV.println(55, 30, "GO");
    delay(20000);
    state = 1;
    newRound = true;
  }
  delay(2000);
  TV.clear_screen();
}

void game() {
  displayScore();
  if (newRound) {
    ballPosition[0] = 118 / 2;
    ballPosition[1] = 96 / 2;
    if (random(0, 2) == 1) {
      ballEndPosition[0] = 118;
    } else {
      ballEndPosition[0] = 0;
    }
    if (random(0, 2) == 1) {
      ballDirection = 1;
    } else {
      ballDirection = 2;
    }
    newRound = false;
  } else {
    ballMovement();
    if (ballPosition[0] == 118) {
      player1Score++;
      newRound = true;
    }
    if (ballPosition[0] == 0) {
      player2Score++;
      newRound = true;
    }
    player1InputVal = /*analogRead(player1Input)*/ 150;
    player1InputVal = map(player1InputVal, 0, 1023, 10, maxPositions[1]);
    TV.draw_rect(0, player1InputVal, 4, 30, WHITE, 1);
    if (ballPosition[0] == 5 && batContact(1, player1InputVal)) {
      ballEndPosition[0] = 118;
    }
    player2InputVal = analogRead(player2Input);
    player2InputVal = map(player2InputVal, 0, 1023, 10, maxPositions[1]);
    TV.draw_rect(118 - 4, player2InputVal, 4, 30, WHITE, 1);

    if (ballPosition[0] == 110 && batContact(2, player2InputVal)) {
      ballEndPosition[0] = 0;
    }
    TV.draw_rect(ballPosition[0], ballPosition[1], 4, 4, WHITE, 1);
    delay(10);
    TV.clear_screen();
  }
}

void winner() {
  TV.select_font(font8x8);
  if (player1Score > player2Score) {
    TV.println(26, 20, "Player 1");
  } else {
    TV.println(26, 20, "Player 2");
  }
  TV.println(4, 40, "YOU'RE WINNER!");
  if (startBlink) {
    TV.select_font(font4x6);
    TV.println(34, 60, "(PRESS START)");
    delay(50);
    if (startButtonState == HIGH) {
      delay(50);
      TV.clear_screen();
      state = 0;
    }
    TV.clear_screen();
  }
}

void intro() {
  state = 0;
}

void displayScore() {
  TV.select_font(font4x6);
  TV.println(0, 0, "Player 1");
  TV.println(50, 0, player1Score);
  TV.println(120 / 2, 0, ":");
  TV.println(70, 0, player2Score);
  TV.println(87, 0, "Player 2");
  if (player1Score == neededScore || player2Score == neededScore) state = 2;
}

bool batContact(int player, int playerPos) {
  int ballNext = ballPosition[1];
  if (player == 1) ballNext - 1;
  if (player == 2) ballNext + 4;

  for (int i = 1; i < batSize[1] + 1; i++) {
    int coveredPixel = playerPos + i;
    if (ballNext == coveredPixel) return true;
  }
  return false;
}

void ballMovement() {
  if (ballPosition[1] == 10) ballDirection = 2;
  if (ballPosition[1] == 94) ballDirection = 1;
  if (ballPosition[0] > ballEndPosition[0]) {
    ballPosition[0]--;
  } else {
    ballPosition[0]++;
  }
  if (ballDirection == 1) ballPosition[1]--;
  if (ballDirection == 2) ballPosition[1]++;
}
